﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LinksApi.Controllers
{
    public class TreeController : ApiController
    {
        private static Dictionary<int, CategoryInfo> repository = new Dictionary<int, CategoryInfo>
        {
            { 1, new CategoryInfo { CategoryId = 1, ParentCategoryId = -1, CategoryName = "Cars", CategoryDesc = "they go fast", DisplayOrder = 1, HasSubcategory = true } },
            { 2, new CategoryInfo { CategoryId = 2, ParentCategoryId = -1, CategoryName = "Animals", CategoryDesc = "they are cute", DisplayOrder = 2, HasSubcategory = true } },
            { 4, new CategoryInfo { CategoryId = 4, ParentCategoryId = 1, CategoryName = "Toyota", CategoryDesc = "red corolla", DisplayOrder = 2, HasSubcategory = false } },
            { 3, new CategoryInfo { CategoryId = 3, ParentCategoryId = 1, CategoryName = "Honda", CategoryDesc = "blue civic", DisplayOrder = 1, HasSubcategory = false } },
            { 5, new CategoryInfo { CategoryId = 5, ParentCategoryId = 2, CategoryName = "Cats", CategoryDesc = "fluffy", DisplayOrder = 1, HasSubcategory = false } },
            { 6, new CategoryInfo { CategoryId = 6, ParentCategoryId = 2, CategoryName = "Dogs", CategoryDesc = "friendly", DisplayOrder = 2, HasSubcategory = false } }

        };
        private static int nextKey = 7;

        public IHttpActionResult GetCategories()
        {
            var result = repository.Values.Where(c => c.ParentCategoryId == -1);

            return Ok(result);
        }

        public IHttpActionResult GetCategories(int categoryId)
        {
            var result = repository.Values.Where(x => x.ParentCategoryId == categoryId);

            return Ok(result);
        }

        [HttpPost]
        public IHttpActionResult SaveCategories([FromBody]IEnumerable<CategoryInfo> categories)
        {
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult CreateCategory([FromBody]CategoryInfo category)
        {
            if (!repository.Values.Contains(category))
            {
                category.CategoryId = nextKey;
                nextKey++;

                if (category.ParentCategoryId == 0)
                    category.ParentCategoryId = -1;

                repository.Add(category.CategoryId, category);
                return Ok(category);
            }
            return Conflict();
        }

        [HttpPut]
        public IHttpActionResult UpdateCategory([FromBody]CategoryInfo category)
        {
            if (!repository.Keys.Contains(category.CategoryId))
                return NotFound();

            repository[category.CategoryId] = category;
            return Ok(category);
        }

        [HttpDelete]
        public IHttpActionResult DeleteCategory([FromBody]CategoryInfo category)
        {
            if (!repository.Keys.Contains(category.CategoryId))
                return NotFound();

            repository.Remove(category.CategoryId);
            return Ok(category);
        }

        public class CategoryInfo
        {
            public int CategoryId { get; set; }
            public int ParentCategoryId { get; set; }
            public string CategoryName { get; set; }
            public string CategoryDesc { get; set; }
            public int DisplayOrder { get; set; }
            public bool HasSubcategory { get; set; }
        }

        public void MockCategories()
        {
            var mockList = new List<CategoryInfo>
                {
                    new CategoryInfo { CategoryId = 1, ParentCategoryId = -1, CategoryName = "Cars", CategoryDesc = "they go fast", DisplayOrder = 1 },
                    new CategoryInfo { CategoryId = 2, ParentCategoryId = -1, CategoryName = "Animals", CategoryDesc = "they are cute", DisplayOrder = 2 }
                };
            foreach (CategoryInfo category in mockList)
            {
                repository.Add(category.CategoryId, category);
            }
        }

        public void MockSubCategories()
        {
            var mockList = new List<CategoryInfo>
                {
                    new CategoryInfo { CategoryId = 3, ParentCategoryId = 1, CategoryName = "Honda", CategoryDesc = "blue civic", DisplayOrder = 1 },
                    new CategoryInfo { CategoryId = 4, ParentCategoryId = 1, CategoryName = "Toyota", CategoryDesc = "red corolla", DisplayOrder = 2 },
                    new CategoryInfo { CategoryId = 5, ParentCategoryId = 2, CategoryName = "Cats", CategoryDesc = "fluffy", DisplayOrder = 1 },
                    new CategoryInfo { CategoryId = 6, ParentCategoryId = 2, CategoryName = "Dogs", CategoryDesc = "friendly", DisplayOrder = 2 }
                };
            foreach (CategoryInfo sub in mockList)
            {
                repository.Add(sub.CategoryId, sub);
            }
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }


        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

    }
}
